import config from 'config'
import { Selector } from 'testcafe'
import ReactSelector from 'testcafe-react-selectors'

const signup = {
  url: `${config.get('pubsweet-server.baseUrl')}/signup`,

  title: ReactSelector('Signup').find('h1'),

  username: Selector('form input[type=text]').nth(0),
  email: Selector('form input[type=text]').nth(1),
  password: Selector('form input[type=password]'),
  submit: Selector('form button'),
  login: Selector('form a'),
}

export default signup
