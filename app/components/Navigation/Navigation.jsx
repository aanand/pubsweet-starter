import React from 'react'
import PropTypes from 'prop-types'
import { LinkContainer } from 'react-router-bootstrap'
import { Navbar, Nav, NavItem, NavbarBrand } from 'react-bootstrap'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import Authorize from 'pubsweet-client/src/helpers/Authorize'
import NavbarUser from 'pubsweet-component-navigation/NavbarUser'
import allActions from 'pubsweet-client/src/actions'

const Navigation = ({ actions, currentUser }) => {
  let logoutButtonIfAuthenticated
  if (currentUser.isAuthenticated) {
    logoutButtonIfAuthenticated = (
      <NavbarUser onLogoutClick={actions.logoutUser} user={currentUser.user} />
    )
  }
  return (
    <Navbar fluid>
      <Navbar.Header>
        <NavbarBrand>
          <a href="/">
            <img alt="pubsweet" src="/assets/pubsweet.jpg" />
          </a>
        </NavbarBrand>
      </Navbar.Header>
      <Nav>
        <LinkContainer to="/manage/posts">
          <NavItem>Posts</NavItem>
        </LinkContainer>
        <Authorize object={{ path: '/users' }} operation="GET">
          <LinkContainer to="/manage/users">
            <NavItem>Users</NavItem>
          </LinkContainer>
        </Authorize>
        <Authorize object={{ path: '/teams' }} operation="GET">
          <LinkContainer to="/manage/teams">
            <NavItem>Teams</NavItem>
          </LinkContainer>
        </Authorize>
      </Nav>
      {logoutButtonIfAuthenticated}
    </Navbar>
  )
}

Navigation.propTypes = {
  actions: PropTypes.shape({
    logoutUser: PropTypes.func.isRequired,
  }).isRequired,
  currentUser: PropTypes.shape({
    user: PropTypes.object,
    isAuthenticated: PropTypes.bool,
  }).isRequired,
}

function mapState(state) {
  return {
    currentUser: state.currentUser,
  }
}

function mapDispatch(dispatch) {
  return {
    actions: bindActionCreators(allActions, dispatch),
  }
}

export default connect(mapState, mapDispatch)(Navigation)
